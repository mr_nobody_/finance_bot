CREATE TABLE IF NOT EXISTS users (
    id SERIAL PRIMARY KEY,
    user_id BIGINT,
    language character varying(32),
    username character varying(255) DEFAULT NULL,
    first_name character varying(255),
    last_name character varying(255) DEFAULT NULL,
    registration_date date DEFAULT timezone('utc'::text, now()),
    default_currency integer DEFAULT 0
);


CREATE TABLE IF NOT EXISTS default_currencies (
    id SERIAL PRIMARY KEY,
    name character varying(32),
    description character varying(255),
    icon character varying(32)
);


CREATE TABLE IF NOT EXISTS default_wallets (
    id SERIAL PRIMARY KEY,
    name character varying(255),
    currency integer DEFAULT 0,
    icon character varying(32)
);


CREATE TABLE IF NOT EXISTS users_wallets (
    id SERIAL PRIMARY KEY,
    user_id BIGINT,
    name character varying(255),
    currency_id integer,
    balance float DEFAULT 0.0,
    icon character varying(32) DEFAULT '💰'
);


ALTER TABLE users
   ADD CONSTRAINT fk_user_currency
   FOREIGN KEY (default_currency)
   REFERENCES default_currencies(id);


ALTER TABLE default_wallets
   ADD CONSTRAINT fk_wallet_currency
   FOREIGN KEY (currency)
   REFERENCES default_currencies(id);


ALTER TABLE users_wallets
   ADD CONSTRAINT fk_users_wallets_currency
   FOREIGN KEY (currency_id)
   REFERENCES default_currencies(id);
