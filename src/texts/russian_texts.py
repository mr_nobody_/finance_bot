CHOOSE_CURRENCY = '💬 Выберите валюту которая будет использоваться по умолчанию 👇'
LANGUAGE_SELECTED_CALL = 'Язык выбран ✅'
LANGUAGE_SELECTED_MSG = 'Язык установлен, теперь необходимо настроить валюту.'
BELARUSIAN_RUBLE = 'Белорусский рубль %s'
AMERICAN_DOLLAR = 'Американский доллар %s'
EURO = 'Евро %s'
CURRENCY_SELECTED_CALL = 'Валюта выбрана ✅'
SHOULD_SELECT_CURRENCY = '⚠️ Для продолжения необходимо выбрать валюту!'
CURRENCY_SELECTED_MSG = 'Валюта выбрана, теперь необходимо настроить кошельки.'
NEXT_TO_CATEGORIES = 'Перейти к выбору категорий ➡️'
NEXT_TO_WALLET = 'Перейти к настройке кошелька ➡️'
CHOOSE_WALLETS = '💬 В кошельках будут храниться все Ваши доходы. \n' \
                 'Вы можете выбрать кошельки которые предлагаются ниже 👇\n' \
                 '💬 Позже можно будет изменить название кошельков и валюту.'
ADD_WALLET = '➕ Добавить кошелёк'
WALLET_CHECKED_CALL = 'Кошелёк выбран ✅'
WALLET_UNCHECKED_CALL = 'Кошелёк удалён ☑️'
WALLET_SELECTED_MSG = 'Кошельки выбраны, теперь необходимо выбрать категории (статьи расходов).'
