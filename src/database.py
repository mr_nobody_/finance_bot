import asyncio
import settings

from aiopg.sa import create_engine
from aiopg.sa.result import RowProxy

from typing import Union, List, NoReturn

loop = asyncio.get_event_loop()
engine = loop.run_until_complete(
    create_engine(user=settings.DATABASE['user'],
                  database=settings.DATABASE['database'],
                  host=settings.DATABASE['host'],
                  password=settings.DATABASE['password']))


async def pg_execute(sql) -> NoReturn:
    async with engine.acquire() as conn:
        await conn.execute(sql)


async def pg_fetchall(sql) -> Union[List[RowProxy], None]:
    async with engine.acquire() as conn:
        result = await conn.execute(sql)
        return await result.fetchall()


async def pg_fetchone(sql) -> NoReturn:
    async with engine.acquire() as conn:
        result = await conn.execute(sql)
        return await result.fetchone()
