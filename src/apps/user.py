import logging

from aiogram import types

from apps.models import users
from apps.data_models import UserData

from database import pg_execute, pg_fetchone

logging.basicConfig(level=logging.INFO)


class UserError(Exception):
    """ Base User Error """
    pass


class UserAlreadyExists(UserError):
    """ User already exists """
    pass


class UserNotFound(UserError):
    """ User not found """
    pass


class User(object):

    def __init__(self, message: types.Message):
        self.msg = message
        self.table = users
        self.user_data = UserData(
            user_id=message.chat.id,
            username=message.chat.username,
            first_name=message.chat.first_name,
            language=message.from_user.language_code,
            last_name=message.chat.last_name
        )

    async def get_user(self) -> UserData:
        user = await pg_fetchone(self.table.select().where(
            self.table.c.user_id == self.msg.chat.id))
        if user:
            return UserData(**user)
        else:
            logging.info('Not found user with user_id: %s' % self.msg.chat.id)
            raise UserNotFound

    async def create_user(self, language) -> UserData:
        user_data = self.user_data.to_dict()
        user_data['language'] = language
        del user_data['id']
        del user_data['registration_date']
        try:
            await self.get_user()
        except UserNotFound:
            await pg_execute(self.table.insert().values(user_data))
            logging.info('Create new user with id: %s' % self.msg.chat.id)
        return UserData(**user_data)

    async def update_currency(self, currency_id):
        await pg_execute(self.table.update().values(
            {'default_currency': currency_id}).where(
            self.table.c.user_id == self.msg.chat.id))

    async def update_language(self, language):
        return await pg_execute(self.table.update().values(
            {'language': language}).where(
            self.table.c.user_id == self.msg.chat.id))

    async def user_texts(self):
        user = await self.get_user()
        if user.language == "russian":
            from texts import russian_texts as text
            return text
        if user.language == "english":
            from texts import english_texts as text
            return text
