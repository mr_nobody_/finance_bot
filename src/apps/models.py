import sqlalchemy as sa

metadata = sa.MetaData()

default_currencies = sa.Table('default_currencies', metadata,
                              sa.Column('id', sa.Integer, primary_key=True),
                              sa.Column('name', sa.String(32), nullable=False),
                              sa.Column('description', sa.String(32), nullable=False),
                              sa.Column('icon', sa.String(32), nullable=False))

users = sa.Table('users', metadata,
                 sa.Column('id', sa.Integer, primary_key=True),
                 sa.Column('user_id', sa.BigInteger, nullable=False),
                 sa.Column('language', sa.String(32), nullable=False),
                 sa.Column('first_name', sa.String(32), nullable=False),
                 sa.Column('last_name', sa.String(255), nullable=True, default=None),
                 sa.Column('registration_date', sa.DateTime, nullable=False, server_default='0000-00-00T00:00:00Z'),
                 sa.Column('username', sa.String(255), nullable=True, default=None),
                 sa.Column('default_currency', sa.Integer, sa.ForeignKey(column=default_currencies.c.id),
                           nullable=True, default=0))

default_wallets = sa.Table('default_wallets', metadata,
                           sa.Column('id', sa.Integer, primary_key=True),
                           sa.Column('name', sa.String(32), nullable=False),
                           sa.Column('currency', sa.Integer, sa.ForeignKey(column=default_currencies.c.id)),
                           sa.Column('icon', sa.String(32), nullable=False))

users_wallets = sa.Table('users_wallets', metadata,
                         sa.Column('id', sa.Integer, primary_key=True),
                         sa.Column('user_id', sa.BigInteger, nullable=False),
                         sa.Column('name', sa.String(255), nullable=False),
                         sa.Column('currency_id', sa.Integer, sa.ForeignKey(column=default_currencies.c.id),
                                   nullable=True, default=0),
                         sa.Column('icon', sa.String(32), nullable=True, default='💰'),
                         sa.Column('balance', sa.Float, nullable=True, default=0.0))
