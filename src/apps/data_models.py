from dataclasses import dataclass


@dataclass
class UserData:
    user_id: int
    first_name: str
    language: str
    last_name: str = None
    username: str = None
    registration_date: str = None
    default_currency: int = 0
    id: int = 0

    def to_dict(self) -> dict:
        return {
            'id': self.id,
            'user_id': self.user_id,
            'first_name': self.first_name,
            'language': self.language,
            'last_name': self.last_name,
            'username': self.username,
            'default_currency': self.default_currency,
            'registration_date': self.registration_date
        }


@dataclass
class WalletData:
    id: int
    user_id: int
    name: str
    currency_id: int
    balance: float
    icon: str

    def to_dict(self) -> dict:
        return {
            'id': self.id,
            'user_id': self.user_id,
            'name': self.name,
            'currency_id': self.currency_id,
            'balance': self.balance,
            'icon': self.icon
        }
