from database import pg_fetchall
from apps.models import default_currencies


class Currency:
    table = default_currencies

    @classmethod
    async def get_currencies(cls):
        return await pg_fetchall(cls.table.select().where(
            cls.table.c.name != 'DEFAULT'))
