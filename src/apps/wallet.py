import logging

from sqlalchemy import and_

from apps.data_models import WalletData
from database import pg_fetchall, pg_fetchone, pg_execute
from apps.models import default_wallets, users_wallets


class WalletError(Exception):
    """ Base Wallet Error """
    pass


class WalletNotFound(WalletError):
    """ Wallet not found """
    pass


class Wallet:
    default_wallets_table = default_wallets
    users_wallets_table = users_wallets

    @classmethod
    async def get_default_wallets(cls):
        return await pg_fetchall(cls.default_wallets_table.select())

    @classmethod
    async def get_specific_user_wallet(cls, user_id, wallet_name) -> WalletData:
        wallet = await pg_fetchone(cls.users_wallets_table.select().where(
            and_(
                cls.users_wallets_table.c.name == wallet_name,
                cls.users_wallets_table.c.user_id == user_id
            )
        ))
        if wallet:
            return WalletData(**wallet)
        else:
            logging.info("Not found wallet '%s' for user %s" % (wallet_name, user_id))
            raise WalletNotFound

    @classmethod
    async def get_users_wallets(cls, user_id):
        return await pg_fetchall(cls.users_wallets_table.select().where(
            cls.users_wallets_table.c.user_id == user_id))

    @classmethod
    async def add_wallet_to_user(cls, user_id, wallet_name, wallet_icon, currency_id):
        try:
            await cls.get_specific_user_wallet(user_id, wallet_name)
        except WalletNotFound:
            await pg_execute(cls.users_wallets_table.insert().values(
                user_id=user_id,
                name=wallet_name,
                icon=wallet_icon,
                currency_id=currency_id
            ))
            logging.info("Added wallet '%s' for user %s" % (wallet_name, user_id))

    @classmethod
    async def remove_wallet_from_user(cls, user_id, wallet_name):
        logging.info("Removed wallet '%s' from user %s" % (wallet_name, user_id))
        return await pg_execute(cls.users_wallets_table.delete().where(
            and_(
                cls.users_wallets_table.c.user_id == user_id,
                cls.users_wallets_table.c.name == wallet_name
            )
        ))

    @classmethod
    async def update_wallet_icon(cls, wallet_id, icon):
        logging.info("Added new icon '%s' to wallet %s" % (icon, wallet_id))
        return await pg_execute(cls.users_wallets_table.update().where(
            cls.users_wallets_table.c.id == wallet_id
        ).values(icon=icon))
