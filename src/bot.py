import time
import json
import logging
import settings

from aiogram import Bot, types
from aiogram.dispatcher import Dispatcher
from aiogram.dispatcher.webhook import SendMessage
from aiogram.utils.executor import start_webhook
from aiogram.utils.exceptions import InvalidQueryID, MessageNotModified
from aiogram.contrib.middlewares.logging import LoggingMiddleware

from apps.user import User, UserNotFound
from apps.currency import Currency
from apps.wallet import Wallet, WalletNotFound

logging.basicConfig(level=logging.INFO)

bot = Bot(token=settings.API_TOKEN)
dp = Dispatcher(bot)
dp.middleware.setup(LoggingMiddleware())


@dp.message_handler(commands=['start'])
async def start_command(message: types.Message):
    await bot.send_chat_action(chat_id=message.chat.id, action='typing')
    try:
        await User(message).get_user()
        return SendMessage(message.chat.id, text='Белорусский рубль \U0001F4B1')
    except UserNotFound:
        if message.from_user.language_code == 'ru':
            welcome_text = 'Привет, %s! \u270b \nВыберите язык:'
        else:
            welcome_text = 'Hi, %s! \u270b \nSelect your language:'
        markup = types.InlineKeyboardMarkup()
        markup.add(types.InlineKeyboardButton('🇷🇺 Русский', callback_data='russian'),
                   types.InlineKeyboardButton('🇺🇸 English', callback_data='english'))
        return SendMessage(chat_id=message.chat.id,
                           text=welcome_text % message.chat.first_name,
                           reply_markup=markup)


@dp.callback_query_handler(lambda call: call.data in ['english', 'russian'])
async def setup_language(call: types.CallbackQuery):
    await bot.send_chat_action(chat_id=call.message.chat.id, action='typing')
    await User(call.message).create_user(call.data)
    texts = await User(call.message).user_texts()
    try:
        await bot.answer_callback_query(call.id, text=texts.LANGUAGE_SELECTED_CALL)
    except InvalidQueryID:
        pass
    await bot.edit_message_text(chat_id=call.message.chat.id,
                                message_id=call.message.message_id,
                                text=texts.LANGUAGE_SELECTED_MSG)
    await bot.send_chat_action(chat_id=call.message.chat.id, action='typing')
    time.sleep(1)
    await setup_currencies(call, operation='send')


async def setup_currencies(call: types.CallbackQuery, operation):
    texts = await User(call.message).user_texts()
    default_currencies = await Currency.get_currencies()
    user = await User(call.message).get_user()
    markup = types.InlineKeyboardMarkup()
    rows = []
    for curr in default_currencies:
        if curr.id == user.default_currency:
            button_text = u'\U0001F518 %s %s' % (curr.name, curr.icon)
        else:
            button_text = u'\u26AA %s %s' % (curr.name, curr.icon)
        rows.append(types.InlineKeyboardButton(button_text, callback_data='currency_%s' % curr.id))
    markup.add(*rows)
    markup.add(types.InlineKeyboardButton(texts.NEXT_TO_WALLET, callback_data='setup_wallets'))
    if operation == 'send':
        await bot.send_message(chat_id=call.message.chat.id,
                               text=texts.CHOOSE_CURRENCY,
                               reply_markup=markup)
    if operation == 'edit':
        try:
            await bot.edit_message_text(chat_id=call.message.chat.id,
                                        message_id=call.message.message_id,
                                        text=texts.CHOOSE_CURRENCY,
                                        reply_markup=markup)
        except MessageNotModified:
            pass


@dp.callback_query_handler(lambda call: call.data.split('_')[0] == 'currency')
async def set_currency(call: types.CallbackQuery):
    currency_id = call.data.split('_')[1]
    await User(call.message).update_currency(currency_id)
    texts = await User(call.message).user_texts()
    try:
        await bot.answer_callback_query(call.id, text=texts.CURRENCY_SELECTED_CALL)
    except InvalidQueryID:
        pass
    await setup_currencies(call, operation='edit')


@dp.callback_query_handler(lambda call: call.data == 'setup_wallets')
async def setup_wallets_handler(call: types.CallbackQuery):
    user = await User(call.message).get_user()
    texts = await User(call.message).user_texts()
    if user.default_currency == 0:
        await bot.answer_callback_query(call.id, text=texts.SHOULD_SELECT_CURRENCY, show_alert=True)
    else:
        try:
            await bot.edit_message_text(chat_id=call.message.chat.id,
                                        message_id=call.message.message_id,
                                        text=texts.CURRENCY_SELECTED_MSG)
        except MessageNotModified:
            pass
        await bot.send_chat_action(chat_id=call.message.chat.id, action='typing')
        time.sleep(1)
        await setup_wallets(call, operation='send')


async def setup_wallets(call: types.CallbackQuery, operation):
    user = await User(call.message).get_user()
    texts = await User(call.message).user_texts()
    default_wallets = await Wallet.get_default_wallets()
    default_wallets_names = []
    users_wallets = await Wallet.get_users_wallets(user.user_id)
    markup = types.InlineKeyboardMarkup()
    for d_wallet in default_wallets:
        d_wallet_name = json.loads(d_wallet.name)[user.language]
        default_wallets_names.append(d_wallet_name)
        try:
            user_wallet = await Wallet.get_specific_user_wallet(user.user_id, d_wallet_name)
            button_text = "%s %s %s\n" % ('✅', user_wallet.name, user_wallet.icon)
            # suw means should unchecked wallet
            callback_data = 'suw_%s_%s_%s' % (user_wallet.name, user_wallet.icon, user_wallet.currency_id)
        except WalletNotFound:
            button_text = "%s %s %s\n" % ('☑️', d_wallet_name, d_wallet.icon)
            # suw means should checked wallet
            callback_data = 'scw_%s_%s_%s' % (d_wallet_name, d_wallet.icon, d_wallet.currency)
        markup.add(types.InlineKeyboardButton(button_text, callback_data=callback_data))
    for u_wallet in users_wallets:
        if u_wallet.name not in default_wallets_names:
            callback_data = 'suw_%s_%s_%s' % (u_wallet.name, u_wallet.icon, u_wallet.currency_id)
            markup.add(types.InlineKeyboardButton("%s %s %s\n" % ('✅', u_wallet.name, u_wallet.icon),
                                                  callback_data=callback_data))
    markup.add(types.InlineKeyboardButton(texts.ADD_WALLET, callback_data='create_wallet'))
    markup.add(types.InlineKeyboardButton(texts.NEXT_TO_CATEGORIES, callback_data='setup_categories'))

    if operation == 'send':
        await bot.send_message(chat_id=call.message.chat.id,
                               text=texts.CHOOSE_WALLETS,
                               reply_markup=markup)
    if operation == 'edit':
        try:
            await bot.edit_message_text(chat_id=call.message.chat.id,
                                        message_id=call.message.message_id,
                                        text=texts.CHOOSE_WALLETS,
                                        reply_markup=markup)
        except MessageNotModified:
            pass


@dp.callback_query_handler(lambda call: call.data.split('_')[0] in ['suw', 'scw'])
async def checked_unchecked_wallet(call: types.CallbackQuery):
    checkbox_value = call.data.split('_')[0]
    wallet_name = call.data.split('_')[1]
    wallet_icon = call.data.split('_')[2]
    wallet_currency = call.data.split('_')[3]
    user = await User(call.message).get_user()
    texts = await User(call.message).user_texts()
    if checkbox_value == 'suw':
        await Wallet.remove_wallet_from_user(user.user_id, wallet_name)
        try:
            await bot.answer_callback_query(call.id, text=texts.WALLET_UNCHECKED_CALL)
        except InvalidQueryID:
            pass
        await setup_wallets(call, operation='edit')

    if checkbox_value == 'scw':
        if wallet_currency == '0':
            wallet_currency = user.default_currency
        await Wallet.add_wallet_to_user(
            user.user_id, wallet_name,
            wallet_icon, wallet_currency)
        try:
            await bot.answer_callback_query(call.id, text=texts.WALLET_CHECKED_CALL)
        except InvalidQueryID:
            pass
        await setup_wallets(call, operation='edit')


async def on_startup(app):
    await bot.set_webhook(settings.WEBHOOK_URL)


async def on_shutdown(app):
    logging.warning('Shutting down...')
    await bot.delete_webhook()
    logging.warning('Bye!')


if __name__ == '__main__':
    start_webhook(
        dispatcher=dp,
        webhook_path=settings.WEBHOOK_PATH,
        on_startup=on_startup,
        on_shutdown=on_shutdown,
        skip_updates=True,
        host=settings.WEBAPP_HOST,
        port=settings.WEBAPP_PORT
    )
